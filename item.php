<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    // meta tag robots
    if( osc_item_is_spam() || osc_premium_is_spam() ) {
        osc_add_hook('header','azzurro_nofollow_construct');
    } else {
        osc_add_hook('header','azzurro_follow_construct');
    }

    osc_enqueue_script('fancybox');
    osc_enqueue_style('fancybox', osc_current_web_theme_url('js/fancybox/jquery.fancybox.css'));
    osc_enqueue_script('jquery-validate');

    azzurro_add_body_class('item');
    osc_add_hook('after-main','sidebar');
    function sidebar(){
        osc_current_web_theme_path('item-sidebar.php');
    }

    $location = array();
    if( osc_item_city_area() !== '' ) {
        $location[] = osc_item_city_area();
    }
    if( osc_item_city() !== '' ) {
        $location[] = osc_item_city();
    }
    if( osc_item_region() !== '' ) {
        $location[] = osc_item_region();
    }
    if( osc_item_country() !== '' ) {
        $location[] = osc_item_country();
    }
    
    if( osc_is_web_user_logged_in()) {
          osc_current_web_theme_path('header.php');
    } else {
          osc_current_web_theme_path('headerp.php');}
?>
<div class="user-card" id="usrhead" style="display:none">
        <div style="height:10px;"></div>
        <div id="usrpic"><img src="<?php echo profilepic_user_url(osc_user_id()); ?>"></div>
        <div id="usrname"><a href="<?php echo osc_user_public_profile_url( osc_item_user_id() ); ?>" ><i class="fas fa-arrow-circle-left"></i> <?php echo osc_user_name(); ?><?php if ( osc_user_phone() != '' ) { ?>
            <?php } ?></a></div>
        <div id="usrphone"><?php printf(__("%s", 'azzurro'), osc_user_phone()); ?></div>
        <div id="usrtopurl" style="display:none"><?php printf(__("%s", 'azzurro'), osc_user_id()); ?></div>
        <div id="usrtype" style="display:none"><?php echo osc_user_region(); ?></div>
    </div>
    <div id="storechat"></div>
    <div id="qrcontainer">
        <div id="qrcode"></div>
        <div id="qrinfo">
        <p>Point your mobile camera on this qr-code to go directly to this page</p>
        <p>وجه كاميرا هاتفك النقال على هذا الرمز للذهاب لهذه الصفحة مباشرة</p>
        </div>
    </div>
<div id="sharebuttons">
        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
        <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
        <a class="a2a_button_facebook"></a>
        <a class="a2a_button_twitter"></a>
        <a class="a2a_button_email"></a>
        </div>
        <script async src="https://static.addtoany.com/menu/page.js"></script>
    </div>
<div id="item-content">
        <h1><?php if( osc_price_enabled_at_items() ) { ?><span class="price" id="itemprice"><?php echo osc_item_formated_price(); ?></span> <?php } ?><strong id="itemname"><?php echo osc_item_title() . ' ' . osc_item_city(); ?></strong></h1>
        <div class="item-header">
            
            <div>
                <?php if ( osc_item_pub_date() !== '' ) { printf( __('<strong class="publish">Published date</strong>: %1$s', 'azzurro'), osc_format_date( osc_item_pub_date() ) ); } ?>
            </div>
            <div>
                <?php if ( osc_item_mod_date() !== '' ) { printf( __('<strong class="update">Modified date:</strong> %1$s', 'azzurro'), osc_format_date( osc_item_mod_date() ) ); } ?>
            </div>
            <?php if (count($location)>0) { ?>
                <ul id="item_location">
                    <li><strong><?php _e("Location", 'azzurro'); ?></strong>: <?php echo implode(', ', $location); ?></li>
                </ul>
            <?php }; ?>
        </div>
        <?php if(osc_is_web_user_logged_in() && osc_logged_user_id()==osc_item_user_id()) { ?>
            <p id="edit_item_view">
                <strong>
                    <a href="<?php echo osc_item_edit_url(); ?>" rel="nofollow"><?php _e('Edit item', 'azzurro'); ?></a>
                </strong>
            </p>
        <?php } ?>


    <?php if( osc_images_enabled_at_items() ) { ?>
        <?php
        if( osc_count_item_resources() > 0 ) {
            $i = 0;
        ?>
        <div class="item-photos">
            <a href="<?php echo osc_resource_url(); ?>" class="main-photo" title="<?php _e('Image', 'azzurro'); ?> <?php echo $i+1;?> / <?php echo osc_count_item_resources();?>">
                <img src="<?php echo osc_resource_preview_url(); ?>" alt="<?php echo osc_item_title(); ?>" title="<?php echo osc_item_title(); ?>" />
            </a>
            <div class="thumbs">
                <?php for ( $i = 0; osc_has_item_resources(); $i++ ) { ?>
                <a href="<?php echo osc_resource_url(); ?>" class="fancybox" data-fancybox-group="group" title="<?php _e('Image', 'azzurro'); ?> <?php echo $i+1;?> / <?php echo osc_count_item_resources();?>">
                    <img src="<?php echo osc_resource_thumbnail_url(); ?>" width="75" alt="<?php echo osc_item_title(); ?>" title="<?php echo osc_item_title(); ?>" />
                </a>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
    <?php } ?>
    <div id="description">
        <p><?php echo osc_item_description(); ?></p>
        <div id="custom_fields">
            <?php if( osc_count_item_meta() >= 1 ) { ?>
                <br />
                <div class="meta_list">
                    <?php while ( osc_has_item_meta() ) { ?>
                        <?php if(osc_item_meta_value()!='') { ?>
                            <div class="meta">
                                <strong><?php echo osc_item_meta_name(); ?>:</strong> <?php echo osc_item_meta_value(); ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <?php osc_run_hook('item_detail', osc_item() ); ?>
        <p class="contact_button">
            <?php if( !osc_item_is_expired () ) { ?>
            <?php if( !( ( osc_logged_user_id() == osc_item_user_id() ) && osc_logged_user_id() != 0 ) ) { ?>
                <?php     if(osc_reg_user_can_contact() && osc_is_web_user_logged_in() || !osc_reg_user_can_contact() ) { ?>
                <?php     } ?>
            <?php     } ?>
            <?php } ?>
        </p>
        <?php osc_run_hook('location'); ?>
    </div>
    <?php if( osc_comments_enabled() ) { ?>
        <?php if( osc_reg_user_post_comments () && osc_is_web_user_logged_in() || !osc_reg_user_post_comments() ) { ?>
        <div id="comments">
            <h2><?php _e('Comments', 'azzurro'); ?></h2>
            <ul id="comment_error_list"></ul>
            <?php CommentForm::js_validation(); ?>
            <?php if( osc_count_item_comments() >= 1 ) { ?>
                <div class="comments_list">
                    <?php while ( osc_has_item_comments() ) { ?>
                        <div class="comment">
                            <h3><strong><?php echo osc_comment_title(); ?></strong> <em><?php _e("by", 'azzurro'); ?> <?php echo osc_comment_author_name(); ?>:</em></h3>
                            <p><?php echo nl2br( osc_comment_body() ); ?> </p>
                            <?php if ( osc_comment_user_id() && (osc_comment_user_id() == osc_logged_user_id()) ) { ?>
                            <p>
                                <a rel="nofollow" href="<?php echo osc_delete_comment_url(); ?>" title="<?php _e('Delete your comment', 'azzurro'); ?>"><?php _e('Delete', 'azzurro'); ?></a>
                            </p>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <div class="paginate" style="text-align: right;">
                        <?php echo osc_comments_pagination(); ?>
                    </div>
                </div>
            <?php } ?>
            <div class="form-container form-horizontal">
                <div class="header">
                    <h3><?php _e('Leave your comment (spam and offensive messages will be removed)', 'azzurro'); ?></h3>
                </div>
                <div class="resp-wrapper">
                    <form action="<?php echo osc_base_url(true); ?>" method="post" name="comment_form" id="comment_form">
                        <fieldset>

                            <input type="hidden" name="action" value="add_comment" />
                            <input type="hidden" name="page" value="item" />
                            <input type="hidden" name="id" value="<?php echo osc_item_id(); ?>" />
                            <?php if(osc_is_web_user_logged_in()) { ?>
                                <input type="hidden" name="authorName" value="<?php echo osc_esc_html( osc_logged_user_name() ); ?>" />
                                <input type="hidden" name="authorEmail" value="<?php echo osc_logged_user_email();?>" />
                            <?php } else { ?>
                                <div class="control-group">
                                    <label class="control-label" for="authorName"><?php _e('Your name', 'azzurro'); ?></label>
                                    <div class="controls">
                                        <?php CommentForm::author_input_text(); ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="authorEmail"><?php _e('Your e-mail', 'azzurro'); ?></label>
                                    <div class="controls">
                                        <?php CommentForm::email_input_text(); ?>
                                    </div>
                                </div>
                            <?php }; ?>
                            <div class="control-group">
                                <label class="control-label" for="title"><?php _e('Title', 'azzurro'); ?></label>
                                <div class="controls">
                                    <?php CommentForm::title_input_text(); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="body"><?php _e('Comment', 'azzurro'); ?></label>
                                <div class="controls textarea">
                                    <?php CommentForm::body_input_textarea(); ?>
                                </div>
                            </div>
                            <div class="actions">
                                <button type="submit"><?php _e('Send', 'azzurro'); ?></button>
                            </div>

                        </fieldset>
                    </form>
<?osc_run_hook( 'disqus_custom_hook', 'disqus_plugin_init_dq' );?>
                </div>
            </div>
        </div>
        <?php } ?>
    <?php } ?>
</div>
<div style="height:120px;text-align:center"><a href="/tac.html"><br>سياسة الإستخدام</a></div>
    <a href="/index.html"><div id="myshuplogo">myshup.com</div></a>
<script>
    window.onload = function() {
  replace();
    }

    function replace() {
    document.getElementById("storechat").innerHTML = "<a href='https://wa.me/"+document.getElementById("usrphone").innerHTML+"?text="+document.getElementById("itemname").innerHTML+'Price:%20'+document.getElementById("itemprice").innerHTML+'%20'+window.location.href+"'>اطلب الآن <i class='fa fa-whatsapp'></i></a>";
    document.getElementById("headerlink").removeAttribute("href");
    document.getElementById("qrcode").innerHTML = '<img src="https://chart.googleapis.com/chart?chs=150x150&amp;cht=qr&amp;chl='+window.location.href+'&amp;choe=UTF-8" alt="QR code">';
    document.getElementById("headerp").innerHTML = document.getElementById("usrhead").innerHTML;
    document.getElementById("headerp").style.backgroundColor = document.getElementById("usrtype").innerHTML;
    document.getElementById("headerp").style.backgroundImage = "linear-gradient(122deg,"+document.getElementById("usrtype").innerHTML+",rgba(0,0,0,0.5))";
    }
</script>
<?php osc_current_web_theme_path('footer.php') ; ?>