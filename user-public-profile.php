<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    // meta tag robots
    osc_add_hook('header','azzurro_follow_construct');

    $address = '';
    if(osc_user_address()!='') {
        if(osc_user_city_area()!='') {
            $address = osc_user_address().", ".osc_user_city_area();
        } else {
            $address = osc_user_address();
        }
    } else {
        $address = osc_user_city_area();
    }
    $location_array = array();
    if(trim(osc_user_city()." ".osc_user_zip())!='') {
        $location_array[] = trim(osc_user_city()." ".osc_user_zip());
    }
    if(osc_user_region()!='') {
        $location_array[] = osc_user_region();
    }
    if(osc_user_country()!='') {
        $location_array[] = osc_user_country();
    }
    $location = implode(", ", $location_array);
    unset($location_array);

    osc_enqueue_script('jquery-validate');

    azzurro_add_body_class('user-public-profile');
    osc_add_hook('after-main','sidebar');
    function sidebar(){
        osc_current_web_theme_path('user-public-sidebar.php');
    }

    if( osc_is_web_user_logged_in()) {
          osc_current_web_theme_path('headerp.php');
    } else {
          osc_current_web_theme_path('headerp.php');}
?>
<div id="item-content">
    <div class="user-card" id="usrhead" style="display:none">
        <div style="height:10px;"></div>
        <div id="usrpic"><img src="<?php echo profilepic_user_url(osc_user_id()); ?>"></div>
        <div id="usrname"><?php echo osc_user_name(); ?><?php if ( osc_user_phone() != '' ) { ?>
            <?php } ?></div>
        <div id="usrphone"><?php printf(__("%s", 'azzurro'), osc_user_phone()); ?></div>
        <div id="usrtopurl" style="display:none"><?php printf(__("%s", 'azzurro'), osc_user_id()); ?></div>
        <div id="usrtype" style="display:none"><?php echo osc_user_region(); ?></div>
    </div>
    <div id="storechat"></div>
    <div class="main-search">
            <div class="cell">
            <input type="text" name="sPattern" id="query" class="input-text" value="" placeholder="Find products إبحث عن منتج" />
            </div>
            <div class="cell reset-padding">
            <div class="cell">
            <button class="ui-button ui-button-big js-submit" onclick="openWin()">Search بحث</button>
            </div>
            </div>
        </div>
    <div id="qrcode"></div>
    <div id="qrinfo">
    <p>Point your mobile camera on this qr-code to go directly to this page</p>
    <p>وجه كاميرا هاتفك النقال على هذا الرمز للذهاب لهذه الصفحة مباشرة</p>
    </div>
    <div id="sharebuttons">
        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
        <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
        <a class="a2a_button_facebook"></a>
        <a class="a2a_button_twitter"></a>
        <a class="a2a_button_email"></a>
        </div>
        <script async src="https://static.addtoany.com/menu/page.js"></script>
    </div>
    <div class="col-md-12">
    <?php if( osc_user_info() !== '' ) { ?>
    <h2><?php _e('User description', 'azzurro'); ?></h2>
    <?php } ?>
    <?php echo nl2br(osc_user_info()); ?>
    </div>
    <?php if( osc_count_items() > 0 ) { ?>
    <div class="similar_ads">
        <h4 class="col-md-12"><?php _e('Latest products أحدث المنتجات', 'azzurro'); ?> <i onclick="zoom()" class="fas fa-search-plus"></i></h4>
        <?php osc_current_web_theme_path('loop.php'); ?>
        <div class="paginate"><?php echo osc_pagination_items(); ?></div>
        <div class="clear"></div>
    </div>
    <?php } ?>
    </div>
    <div style="height:120px;text-align:center"><a href="/tac.html"><br>سياسة الإستخدام</a></div>
    <a href="/index.html"><div id="myshuplogo">myshup.com</div></a>
<script>
    window.onload = function() {
  replace();
    }

    function replace() {
    document.getElementById("storechat").innerHTML = "<a href='https://wa.me/"+document.getElementById("usrphone").innerHTML+"' style='color:#084'>تواصل معنا <i class='fa fa-whatsapp'></i></a>";
    document.getElementById("headerlink").removeAttribute("href");
    document.getElementById("qrcode").innerHTML = '<img src="https://chart.googleapis.com/chart?chs=150x150&amp;cht=qr&amp;chl='+window.location.href+'&amp;choe=UTF-8" alt="QR code">';
    document.getElementById("headerp").innerHTML = document.getElementById("usrhead").innerHTML;
    document.getElementById("headerp").style.backgroundColor = document.getElementById("usrtype").innerHTML;
    document.getElementById("headerp").style.backgroundImage = "linear-gradient(122deg,"+document.getElementById("usrtype").innerHTML+",rgba(0,0,0,0.5))";
    }
    function openWin() {
  myWindow = window.open("https://myshup.com/search/seller_post,"+document.getElementById("usrtopurl").innerHTML+"/pattern,"+document.getElementById("query").value,"_self");   // Opens a new window
}
    function zoom() {
        
    }
</script>
<?php osc_current_web_theme_path('footer.php') ; ?>
