<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */
?>
<div id="sidebar">
    <?php echo osc_private_user_menu( get_user_menu() ); ?>
    <div id="user_menu2"><a href="<?php echo osc_item_post_url_in_category() ; ?>"><?php _e('Add product <i class="fas fa-plus"></i> أضف منتج', 'azzurro');?></a></div>
    <div id="user_menu3"><a href="<?php echo osc_user_logout_url(); ?>"><?php _e('Logout <i class="fas fa-sign-out-alt"></i> تسجيل الخروج', 'azzurro'); ?></a></div>
</div>
