<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    // meta tag robots
    osc_add_hook('header','azzurro_follow_construct');

    azzurro_add_body_class('home');


    $buttonClass = '';
    $listClass   = '';
    if(azzurro_show_as() == 'gallery'){
          $listClass = 'listing-grid';
          $buttonClass = 'active';
    }
?>

<?php osc_current_web_theme_path('header.php') ; ?>
<div class="clear"></div>
    <div id="welcomeen">Welcome to myshup.com<br><br>This website is intended to make it easier for companies, stores, shops, service providers and restaurants to have their own sites online and showcase their products. It also provides the easiest purchase method with a one click purchase. Please know that this is a public beta and is launching in Jordan only ahead of schedule for free to enable shops manage their stores online during the corona crysis.<br>To register click on the Add Products button and go to Register Your Company<br><br>Thank you<br><br>Eng. Mohammed Taher<br>CEO and co-founder</div><div id="welcomear">مرحبا بك في موقع ماي شوب<br><br>هذا الموقع مخصص لتسهيل عملية البيع الإلكتروني للشركات و المحلات و الخدمات و المطاعم و حصولهم على موقعهم الخاص. كما يسهل عملية البيع و الشراء عن طريق الشراء بكبسة واحدة. يرجى العلم أن عذه نسخة تجريبية مجانية و تم إطلاقها قبل موعدها في الأردن لتمكين المحلات من إدارة أعمالهم على الشبكة خلال أزمة الكورونا <br>للتسجيل إضغط على أضف منتج في الأعلى ثم اذهب لسجل شركتك<br><br> شكرا لكم <br><br> م. محمد طاهر <br> المدير التنفيذي</div>
<div class="latest_ads" style="display:none">
 <?php if( osc_count_latest_items() == 0) { ?>
    <div class="clear"></div>
    <p class="empty"><?php _e("There aren't listings available at this moment", 'azzurro'); ?></p>
<?php } else { ?>
    <div class="filters">
</div>
    <div class="actions">
      <span class="doublebutton <?php echo $buttonClass; ?>">
           <a href="<?php echo osc_base_url(true); ?>?sShowAs=list" class="list-button" data-class-toggle="listing-grid" data-destination="#listing-card-list"><span><?php _e('List', 'azzurro'); ?></span></a>
           <a href="<?php echo osc_base_url(true); ?>?sShowAs=gallery" class="grid-button" data-class-toggle="listing-grid" data-destination="#listing-card-list"><span><?php _e('Grid', 'azzurro'); ?></span></a>
      </span>
    </div>
    <?php
    View::newInstance()->_exportVariableToView("listType", 'latestItems');
    View::newInstance()->_exportVariableToView("listClass",$listClass);
    osc_current_web_theme_path('loop.php');
    ?>
    <div class="clear"></div>
    <?php if( osc_count_latest_items() == osc_max_latest_items() ) { ?>
        <p class="see_more_link"><a href="<?php echo osc_search_show_all_url() ; ?>">
            <strong><?php _e('More المزيد', 'azzurro') ; ?> &raquo;</strong></a>
        </p>
    <?php } ?>
<?php } ?>
</div>
</div><!-- main -->

<div class="clear"><!-- do not close, use main clossing tag for this case -->
<?php if( osc_get_preference('homepage-728x90', 'azzurro') != '') { ?>
<!-- homepage ad 728x60-->
<div class="vista_728">
    <?php echo osc_get_preference('homepage-728x90', 'azzurro'); ?>
</div>
<!-- /homepage ad 728x60-->
<?php } ?>
<?php osc_current_web_theme_path('footer.php') ; ?>