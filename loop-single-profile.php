<?php $size = explode('x', osc_thumbnail_dimensions()); ?>
<li class="listing-card <?php echo $class; if(osc_item_is_premium()){ echo ' premium'; } ?>">
<?php if( osc_images_enabled_at_items() ) { ?>
<?php if(osc_count_item_resources()) { ?>

<a class="listing-thumb" href="<?php echo osc_user_public_profile_url( osc_item_user_id() ); ?>" title="<?php echo osc_esc_html(osc_item_title()) ; ?>"><img src="<?php echo profilepic_user_url(osc_user_id()); ?>">" title="" alt="<?php echo osc_esc_html(osc_item_title()) ; ?>" width="<?php echo $size[0]; ?>" height="<?php echo $size[1]; ?>"></a>
<?php } else { ?> <a class="listing-thumb" href="<?php echo osc_user_public_profile_url( osc_item_user_id() ); ?>" title="<?php echo osc_esc_html(osc_item_title()) ; ?>"><img src="<?php echo osc_current_web_theme_url('images/no_photo.gif'); ?>" title="" alt="<?php echo osc_esc_html(osc_item_title()) ; ?>" width="<?php echo $size[0]; ?>" height="<?php echo $size[1]; ?>"></a>
<?php } ?><?php } ?>


<div class="listing-detail">

<div class="listing-cell">

<div class="listing-data">

<div class="listing-basicinfo">
<span class="storename"><a href="<?php echo osc_user_public_profile_url( osc_item_user_id() ); ?>" ><?php echo osc_item_contact_name(); ?></a></span>
</div>
</div>
</li>
