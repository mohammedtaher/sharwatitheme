<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    // meta tag robots
    osc_add_hook('header','azzurro_follow_construct');

    $address = '';
    if(osc_user_address()!='') {
        if(osc_user_city_area()!='') {
            $address = osc_user_address().", ".osc_user_city_area();
        } else {
            $address = osc_user_address();
        }
    } else {
        $address = osc_user_city_area();
    }
    $location_array = array();
    if(trim(osc_user_city()." ".osc_user_zip())!='') {
        $location_array[] = trim(osc_user_city()." ".osc_user_zip());
    }
    if(osc_user_region()!='') {
        $location_array[] = osc_user_region();
    }
    if(osc_user_country()!='') {
        $location_array[] = osc_user_country();
    }
    $location = implode(", ", $location_array);
    unset($location_array);

    osc_enqueue_script('jquery-validate');

    azzurro_add_body_class('user-public-profile');
    osc_add_hook('after-main','sidebar');
    function sidebar(){
        osc_current_web_theme_path('user-public-sidebar.php');
    }

    osc_current_web_theme_path('header.php');
?>
<div id="item-content">
    <div class="user-card" style="display:none">
        <img src="http://www.gravatar.com/avatar/<?php echo md5( strtolower( trim( osc_user_email() ) ) ); ?>?s=120&d=<?php echo osc_current_web_theme_url('images/user_default.gif') ; ?>" />
        <ul id="user_data">
            <li class="name" id="logoreplace"><?php echo osc_user_name(); ?><?php if ( osc_user_phone() != '' ) { ?>
            <?php } ?></li>
            <?php if( osc_user_website() !== '' ) { ?>
            <li class="website"><a href="<?php echo osc_user_website(); ?>"><?php echo osc_user_website(); ?></a></li>
            <?php } ?>
            <?php if( $address !== '' ) { ?>
            <li class="adress"><?php printf(__('<strong>Address:</strong> %1$s'), $address); ?></li>
            <?php } ?>
            <?php if( $location !== '' ) { ?>
            <li class="location"><?php printf(__('<strong>Location:</strong> %1$s'), $location); ?></li>
            <?php } ?>
        </ul>
        <div class="phone" id="phonenum"><?php printf(__("%s", 'azzurro'), osc_user_phone()); ?></div>
    </div>
    <div id="strspacer"></div>
    <div class="col-md-12">
    <?php if( osc_user_info() !== '' ) { ?>
    <h2><?php _e('User description', 'azzurro'); ?></h2>
    <?php } ?>
    <?php echo nl2br(osc_user_info()); ?>
    </div>
    <?php if( osc_count_items() > 0 ) { ?>
    <div class="similar_ads">
        <h2 class="col-md-12"><?php _e('Latest products أحدث المنتجات', 'azzurro'); ?></h2>
        <?php osc_current_web_theme_path('loop.php'); ?>
        <div class="paginate"><?php echo osc_pagination_items(); ?></div>
        <div class="clear"></div>
    </div>
    <?php } ?>
    </div>
    <div style="height:50px"></div>
    <div id="qrcode"></div>
    <div id="profilepic"><img src="<?php echo profilepic_user_url(osc_item_user_id()); ?>"></div>
    <a href="/index.php"><div id="myshuplogo"><br>الموقع غير مسؤول عن المنتجات المعروضة</div></a>
<script>
    window.onload = function() {
  replace();
    }

    function replace() {
      document.getElementById("storechatreplacer").innerHTML = "<a style='color:#fff!important;position:fixed;bottom:0;right:0;background:#084;padding:15px;border-radius: 20px 0 0 0' href='https://wa.me/"+document.getElementById("phonenum").innerHTML+"' style='color:#084'>Whatsapp <i class='fa fa-whatsapp'></i></a>";
    document.getElementById("logo").style = "background-image:none;width:100%;margin-top:30px;";
    document.getElementById("headerlink").removeAttribute("href");
    document.getElementById("header").id = "strhead";
    document.getElementById("logo").innerHTML = "<div id='strname'>"+document.getElementById("logoreplace").innerHTML+"</div><a href='/index.php'></a><div id='strnum'><i class='fa fa-mobile'></i> "+document.getElementById("phonenum").innerHTML+"</div>";
    document.getElementById("qrcode").innerHTML = '<img src="https://chart.googleapis.com/chart?chs=150x150&amp;cht=qr&amp;chl='+window.location.href+'&amp;choe=UTF-8" alt="QR code">';
    }
</script>
<?php osc_current_web_theme_path('footer.php') ; ?>
