<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    // meta tag robots
    osc_add_hook('header','azzurro_nofollow_construct');

    azzurro_add_body_class('user user-profile');
    osc_add_hook('before-main','sidebar');
    function sidebar(){
        osc_current_web_theme_path('user-sidebar.php');
    }
    osc_add_filter('meta_title_filter','custom_meta_title');
    function custom_meta_title($data){
        return __('Update account', 'azzurro');
    }
    osc_current_web_theme_path('header.php') ;
    $osc_user = osc_user();
?>
<h1><?php _e('Update account', 'azzurro'); ?></h1>
<?php UserForm::location_javascript(); ?>
<div class="form-container form-horizontal" id="user-profile-form">
    <div class="resp-wrapper">
        <ul id="error_list"></ul>
        <form action="<?php echo osc_base_url(true); ?>" method="post">
            <input type="hidden" name="page" value="user" />
            <input type="hidden" name="action" value="profile_post" />
            <div class="control-group">
                <label class="control-label" for="name"><?php _e('Store name اسم المتجر', 'azzurro'); ?></label>
                <div class="controls">
                    <?php UserForm::name_text(osc_user()); ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="name"><?php _e('Cover color لون الترويسة', 'azzurro'); ?></label>
                <div class="controls">
                    <?php UserForm::region_text(osc_user()); ?>
                </div>
                <canvas id="picker"></canvas>
                <div id="headpreview">Your Name</div>
            </div>
            <div class="control-group">
                <label class="control-label" for="phoneMobile"><?php _e('Whatsapp cell phone in the form (962777777777) موبايل للواتساب بالطريقة', 'azzurro'); ?></label>
                <div class="controls">
                    <?php UserForm::mobile_text(osc_user()); ?>
                </div>
            </div>
            
            <?php osc_run_hook('user_profile_form', osc_user()); ?>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="ui-button ui-button-middle ui-button-main"><?php _e("Update", 'azzurro');?></button>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <?php osc_run_hook('user_form', osc_user()); ?>
                </div>
            </div>
        </form>
    </div>
    </div>
</div>
<script>
new KellyColorPicker({place : 'picker', input : 'region', size : 150});
      window.onload = function() {
  replace();
    }

    function replace() {
    document.getElementById("headpreview").style.backgroundImage = "linear-gradient(122deg,"+document.getElementById("region").value+",#000)";
    }
</script>
<?php osc_current_web_theme_path('footer.php') ; ?>
